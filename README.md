# The following script will take Haploid VCF files and create Diploid genotypes based on Phred-score Likelihood calculations.
# That is to say it will take a allele such as 'A' and make it 'AA' or 'AG'
# FORMAT=PL,Number=3,Type=Float,Description="Normalized, Phred-scaled likelihoods for AA,AB,BB genotypes where A=ref and B=alt; not applicable if site is not biallelic">
# Phred-scaled probability (between 0 to 255)  1 – 10(255/-10) 
# P(D|TG) = 10^(0) = 1
